package com.adit.themeal.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.adit.themeal.DetailActivity
import com.adit.themeal.databinding.ItemsMealsBinding
import com.adit.themeal.model.ModelMeal
import com.bumptech.glide.Glide

class MainAdapter: RecyclerView.Adapter<MainAdapter.ViewHolder>() {
    class ViewHolder(private val binding: ItemsMealsBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(data: ModelMeal){
            binding.apply {
                Glide.with(itemView.context)
                    .load(data.imageMeal)
                    .into(imageMeal)

                nameMeal.text = data.nameMeal

                itemView.setOnClickListener {
                    val intent = Intent(itemView.context,DetailActivity::class.java)
                    intent.putExtra(DetailActivity.MEALS_ID,data.idMeal.toString())
                    itemView.context.startActivity(intent)
                }
            }
        }
    }

    private var listMeals = ArrayList<ModelMeal>()

    fun setData(items: ArrayList<ModelMeal>?){
        if(items?.isEmpty() == true) return
        listMeals.clear()
        if (items != null) {
            listMeals.addAll(items)
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemsMealsBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listMeals[position])
    }

    override fun getItemCount(): Int = listMeals.size
}