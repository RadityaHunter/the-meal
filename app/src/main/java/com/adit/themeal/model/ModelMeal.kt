package com.adit.themeal.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

data class ResMeal(
    @SerializedName("meals") var meals: ArrayList<ModelMeal>
)
@Parcelize
data class ModelMeal(
    @SerializedName("idMeal") var idMeal: Int,
    @SerializedName("strMeal") var nameMeal: String?=null,
    @SerializedName("strMealThumb") var imageMeal: String?=null,
    @SerializedName("strInstructions") var instructionsMeal:String?=null
) : Parcelable
