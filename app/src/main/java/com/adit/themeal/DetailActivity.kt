package com.adit.themeal

import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.adit.themeal.databinding.ActivityDetailBinding
import com.adit.themeal.model.ModelMeal
import com.adit.themeal.mvvm.MainViewModel
import com.bumptech.glide.Glide

class DetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailBinding
    private val detailViewModel: MainViewModel by viewModels()
    private var dataMeals: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getDetailData()
    }

    private fun getDetailData() {
        dataMeals = intent.getStringExtra(MEALS_ID)
        if (dataMeals != null) {
            setViewModel(id = dataMeals.toString())
            Log.d("CHECK ID", dataMeals.toString())
        }

    }

    private fun setViewModel(id: String) {
        detailViewModel.getDetailMealById(id).observe(this, { data ->
            if (data != null) {
                getDataFromNetwork(data)
            }
        })
    }

    private fun getDataFromNetwork(data: ModelMeal) {
        binding.nameMeal.text = data.nameMeal
        binding.instructionsMeal.text = data.instructionsMeal

        Glide.with(this)
            .load(data.imageMeal)
            .into(binding.imageMeal)

    }

    companion object {
        const val MEALS_ID = "MEALS_ID"
    }
}