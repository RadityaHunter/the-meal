package com.adit.themeal.network

import com.adit.themeal.model.ResMeal
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiMeals {
    @GET("filter.php?c=Seafood")
    fun getListMeal(): Call<ResMeal>

    @GET("lookup.php")
    fun getDetailMeals(
        @Query("i") keyId: String
    ): Call<ResMeal>
}