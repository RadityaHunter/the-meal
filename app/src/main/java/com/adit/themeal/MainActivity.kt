package com.adit.themeal

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.adit.themeal.adapter.MainAdapter
import com.adit.themeal.databinding.ActivityMainBinding
import com.adit.themeal.mvvm.MainViewModel

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var adapter: MainAdapter
    private val mainViewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        adapter = MainAdapter()

        setRecyclerView()
        setViewModel()
    }

    private fun setViewModel(){
        mainViewModel.getAllMeals().observe(this,{
            adapter.setData(it)
        })
    }

    private fun setRecyclerView(){
        binding.rvListMeals.layoutManager = GridLayoutManager(this,2)
        binding.rvListMeals.setHasFixedSize(true)
        binding.rvListMeals.adapter = adapter
    }
}