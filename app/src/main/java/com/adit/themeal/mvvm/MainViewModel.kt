package com.adit.themeal.mvvm

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.adit.themeal.model.ModelMeal
import com.adit.themeal.model.ResMeal
import com.adit.themeal.network.NetworkMeals
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainViewModel : ViewModel() {
    private val _getAllMeals = MutableLiveData<ArrayList<ModelMeal>>()
    private val _getDetailMeal = MutableLiveData<ModelMeal>()

    fun getAllMeals(): LiveData<ArrayList<ModelMeal>> {
        NetworkMeals.getApiService().getListMeal().enqueue(object : Callback<ResMeal> {
            override fun onResponse(call: Call<ResMeal>, response: Response<ResMeal>) {
                if (response.isSuccessful) {
                    _getAllMeals.postValue(response.body()?.meals)
                }
            }

            override fun onFailure(call: Call<ResMeal>, t: Throwable) {
                Log.d("ON FAILURE", t.message.toString())
            }

        })
        return _getAllMeals
    }

    fun getDetailMealById(id:String): LiveData<ModelMeal> {
        NetworkMeals.getApiService().getDetailMeals(id).enqueue(object : Callback<ResMeal> {
            override fun onResponse(call: Call<ResMeal>, response: Response<ResMeal>) {
                if(response.isSuccessful){
                    _getDetailMeal.postValue(response.body()?.meals?.get(0))
                }
            }

            override fun onFailure(call: Call<ResMeal>, t: Throwable) {
                Log.d("ON FAILURE",t.message.toString())
            }

        })
        return _getDetailMeal
    }

}